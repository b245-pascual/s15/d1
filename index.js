//sections comments

 //comments are parts of the code that gets ignored by the language.
// there are 2types of comment
// single line comment (ctrl+/)denoted by code
/*
	-multiline comment
	-denoted by 
	-ctrl shift */
//[section] statements and syntax
	// Statements
		// In programming language, statements are intrucstions that we tell the computer to perform.
		
		// JS statemens usually ends with semicolon (;).
			// (;) is also called as delimeter.
		// Semicolons are not required in JS.
			// it use to help us train or locate where a statement ends.
	// Syntax -paranggrammar
		// In programming, it is the set of rules that describes how statements must be constructed.
		

console.log("Hello World")
//  [Secion] Variables -parang lunch box pwede mag store anyfood

// it is used to contain data.

// Declaring a variable
	// tells our devices that a variable name is created and is ready to store data.

	// Syntax: let/const variableName;

		// let is a keyword that is usally used in a declaring variable
		let myVariable;
		console.log(myVariable); //useful for printing a values or a variable

		// console.log(Hello); //error: not defined

		// 1 Initialize/initialization a value
			// Storing the intial/starting value of a variable.
			// Assignment Operator(=)
		myVariable = "Hello";

		//2 Reassignment a variable value
			//changing the initial value to a new value.
		myVariable = "Hello World";
		console.log(myVariable)

	// const
		// "const" keyword is used to declare and initialized a constant variable.
		//A constant variable does not change
	// const PI;
	// PI = 3.1416;
	// console.log(PI); //error: Initial value is missing for const.

//Declarating with initialization
	// a variable is given it's initial or starting value upon declarion.
		// Syntax: let/const variableName = value;

		//let keyword
		let productName = "Desktop Computer";
		console.log(productName);

		let productPrice = 18999;
		// let productPrice = 20000;
		console.log(productPrice);
 
 		//const keyword
 		const PI = 3.1416;
 		// PI 3.14; error: assignment to constant variable
/*
	Guide in writing variables.
	1. use the "let" keyword if the variable will contain different values can change over time.
		Naming Convention: variable name shoud starts with "lowercase characters" and "camelCasing" is use for multiple words.

	2. use the "const" keyword if the variable does not changed.
		Naming Convention: "const" keyword should be follow by the all CAPITAL VARIABLE NAME (single value variable).

	3. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

	Avoid this one: let word = "John Smith"

	4. Variable name are case sensitive.

*/

//Multipe variable declaration 

 		let productCode = "DC017", productBrand = "Dell";
 		console.log(productCode, productBrand);

//Using a reserve keyword

 		// const let = "Hello";
 		// console.log(let); error: lexically bound is disallowed.

//[SECTION] Data Types 

//In JavasScript, there are six types of data

 	// String 
 		//are series of character that create a word, a phrase a sentence or anything related to creating text.
 		// Strings in JavaScript can be writtend using a single quote ('')or double quote ("")

 		let country = "Philippines"
 		let province = 'Metro Manila';

 		//Concatenating Strings
 		// Multiple string values that can be combined to create a single String using the "+" symbol.
 		//I live in Metro Manila, Philippines
 		let greeting = "I live in " + province + "," + country
 		console.log(greeting);

 		// Escape Characters
 		// (\) in string combination with other characters can produce different result
 		let mailAddress = "Quezon City\nPhilippines" 
 		console.log(mailAddress);

 		//Expected output: John's employess went home early.
 		let message = "John's employees went home early.";
 		console.log(message);

 		//using the escape characters (/)
 		message = 'John\'s employees went home early.';
 		console.log(message)

 		// Numbers
 			//includes positive, negative, or numbers with decimal places.

 			//Integers/Whole Numbers
 			let headcount = 26;
 			console.log(headcount);

 			// Decimal Number/Fractions
 			let grade = 98.7
 			console.log(grade);

 			//Exponential Notation
 			let planetDistance = 2e10;
 			console.log(planetDistance);
    		
    		//combine text and strings
    		console.log("John's grade last quartes is "+grade);

    // Boolean
    	// Boolean values are normally used to stored values relating to the state of certain things.
    	//true or false (two logical values)

    	let isMarried = false; 
    	let isGoodConduct = true;

    	console.log("isMarried: "+isMarried)
    	console.log("isGoodConduct "+isGoodConduct);

    // Array
    	// are special kind of data tpye that can used to store multiple related values.
    	// Syntax: let/const arrayName = [elementA, elementB, ... elementNth];

    	const grades = [98.7, 92.1, 90.2, 94.6];
    	//pwede itanong sa interview 
    	//constant variable cannot be reassigned.
    	// grades= 100; // not allowed
    	//changing the elements of an array or changing the properties of an oject is allowed in constant variable.
    	grades[0] = 100, // reference/index value
    	console.log(grades) 

    	//This will work, but not recommended.
    	let details = ["John", "smith", 32 , true];
    	console.log(details)

   // Objects
   	// objects are another special kind of data type that is used to mimic real objects/items.
    // used to create complex data that contains information relevant to each other.
    /*
		Syntex:
			let/const ObjectName ={
				propertyA: valueA,
				propertyB: valueB,
			}
    */
    let person = {
    	fullname: "Juan Dela Cruz",
    	age: 35,
    	isMarried:false,
    	contact: ["+63912 345 6789", "8123 456"], 
    	address: {
    		houseNumber: "345",
    		city: "Manila"
    	}	
	};

    console.log(person);








    // They're are also useful for creating abstract objects
    let myGrades = {
    	firstGrading: 98.7,
    	secondGrading: 92.1,
    	thirdGrading: 90.2,
    	fourthGrading: 94.6
    }

    console.log(myGrades);

    //typeof operator is used to determined the type of data or value of a variable.

    console.log(typeof grades); // object


    // arrays is a special type of object with methods.
    console.log(typeof grades); //object 

    // Null 
    	// indicates the absence of a value.
    	let spouse = null;
    	console.log(spuse);

    // Undefined
    	//  indicates that a variables has not been given a value yet.
    	let fullname;
    	console.log(fullName);

